#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <string.h>
#include <net/if.h>  
#include <linux/mii.h>
#include <linux/sockios.h>
#include <linux/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>


#define DEVICE0_ID        0x10
#define DEVICE0_CPU_PORT  0x5



#define	DEV_ID		0x10

#define PHY_CREG				0
#define PHY_SREG				1
#define PHY_ID1_REG				2
#define PHY_ID2_REG				3
#define PHY_ANEG_ADV_REG		4




#define 	EMAC_REG(reg)		(*(volatile unsigned long *)(ETH_BASEADDR + reg))
#define IN 0
#define OUT 1

int sockfd = -1;
int md_init = 0;
void WritePhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long value);
void ReadPhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long *value);

void init_mdio (void)
{
	unsigned long data[2];
	char Interface[100];
    //printf("**** init mdio ****\n");
	md_init = 1;

	// create socket and bind to interface
	sockfd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	strncpy(Interface, "eth0", IFNAMSIZ);
	setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, Interface, sizeof(Interface));

	// read Device ID
	ReadPhyReg(DEV_ID, PHY_ID1_REG, &data[0]);
	ReadPhyReg(DEV_ID, PHY_ID2_REG, &data[1]);

	printf ("ETH ID: 0x%04x 0x%04x\r\n", (data[0] & 0xFFFF), (data[1] & 0xFFFF));

	//// set speed to 100 mbps
	// ReadPhyReg(DEV_ID, PHY_CREG, &data[0]);
	// printf ("PHY_CREG: 0x%04x 0x%04x 0x%x\r\n", DEV_ID, PHY_CREG , data[0]);
	// data[0] |= (1<<13); // speed selection = 1 (100 mbps)
	// WritePhyReg(DEV_ID, PHY_CREG, data[0]);
}

#define REG_R "/sys/devices/platform/neta/switch/reg_r"
#define REG_W "/sys/devices/platform/neta/switch/reg_w"

void WritePhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long value)
{
	// if (md_init == 0)
	// 	init_mdio();

    struct ifreq ifr;
    struct mii_ioctl_data *mii;
    int fd, len;
    char buf[10],val[10];
    
    fd = open(REG_W , O_WRONLY);
    if (fd < 0) {
        perror("reg r");
        return fd;
    }

    len = snprintf(buf, sizeof(buf), "%d %d 10 %d", dev_id, registerIndex ,value);
    write(fd, buf, len);
    close(fd);

  	//printf (">>> WritePhyReg %d %02x: %04x\n", registerIndex, value);
	
}
#define SYSFS_SWITCH_DIR "/sys/devices/platform/neta/switch/stats"

unsigned short GetValue(void)
{
    int fd, len,i;
    char buf[256],data[1000];
    char ch;
    unsigned short value = 0;
    
    len = snprintf(buf, sizeof(buf),"/sys/devices/platform/neta/switch/stats");
    //len = snprintf(buf, sizeof(buf),"/sys/devices/platform/neta/switch/status");

    fd = open(buf, O_RDONLY);
    if (fd < 0) {
        perror("switch-value");
        return fd;
    }

    read(fd, data, sizeof(unsigned short ));
    //memset(data,0,100);
    //read(fd, data, 1000);
    memcpy (&value , data ,sizeof(unsigned short ));
    //printf("\n************** GetValue: %X ->  %x %x\n", value,  data[0],data[1]);
    close(fd);
    return value;
}


void ReadPhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long *value)
{
	// if (md_init == 0)
	// 	init_mdio();
		// external PHY
	struct ifreq ifr;
	struct mii_ioctl_data *mii;
    int fd, len;
    char buf[10];//,val[10];
    memset(value,0,sizeof(unsigned long));
    unsigned short  val;

    fd = open(REG_R , O_WRONLY);
    if (fd < 0) {
        perror("reg r");
        return fd;
    }

    len = snprintf(buf, sizeof(buf), "%d %d 10", dev_id, registerIndex);
    write(fd, buf, len);
    close(fd);
    usleep(100000);
    val = GetValue();
    // memcpy(&val, buf, sizeof(unsigned short ));
     memcpy(value, &val, sizeof(unsigned short ));
  
	////printf ("\n\r<<< **** ReadPhyReg %d reg:%02x value:%04x *****\n", dev_id , registerIndex, val);
	
}



void usage(void)
{
	printf("*argv[0] -a <MDIO_ADDRESS> -r <MDIO_REGISTER> -i|-o <VALUE>\n");
	printf("    -a <MDIO_ADDR>   MDIO physical address\n");
	printf("    -r <MDIO_REG>    MDIO register address\n");
	printf("    -i               Input from MDIO\n");
	printf("    -o <VALUE>       Output to MDIO\n");
	return;
}






#include <Copyright.h>
/********************************************************************************
* ev96122mii.c
*
* DESCRIPTION:
*       SMI access routines for EV-96122 board
*
* DEPENDENCIES:   Platform.
*
* FILE REVISION NUMBER:
*
*******************************************************************************/

#include <msSample.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * For each platform, all we need is 
 * 1) Assigning functions into 
 *         fgtReadMii : to read MII registers, and
 *         fgtWriteMii : to write MII registers.
 *
 * 2) Register Interrupt (Not Defined Yet.)
*/

/* 
 *  EV-96122 Specific Definition
*/

//#define DEBUG_QD	0

////#ifdef OLD_BS


#define SMI_OP_CODE_BIT_READ                    1
#define SMI_OP_CODE_BIT_WRITE                   0
#define SMI_BUSY                                1<<28
#define READ_VALID                              1<<27

#ifdef FIREFOX
#define ETHER_SMI_REG                   0x10 
#define internalRegBaseAddr 0x80008000
#define NONE_CACHEABLE        0x00000000
#define CACHEABLE            0x00000000
#define SMI_RX_TIMEOUT        1000
#else
#define ETHER_SMI_REG                   0x080810 
#define internalRegBaseAddr 0x14000000
#define NONE_CACHEABLE        0xa0000000
#define CACHEABLE            0x80000000
#define SMI_RX_TIMEOUT        10000000
#endif

typedef unsigned int              SMI_REG;

#ifdef LE /* Little Endian */              
#define SHORT_SWAP(X) (X)
#define WORD_SWAP(X) (X)
#define LONG_SWAP(X) ((l64)(X))

#else    /* Big Endian */
#define SHORT_SWAP(X) ((X <<8 ) | (X >> 8))

#define WORD_SWAP(X) (((X)&0xff)<<24)+      \
                    (((X)&0xff00)<<8)+      \
                    (((X)&0xff0000)>>8)+    \
                    (((X)&0xff000000)>>24)

#define LONG_SWAP(X) ( (l64) (((X)&0xffULL)<<56)+               \
                            (((X)&0xff00ULL)<<40)+              \
                            (((X)&0xff0000ULL)<<24)+            \
                            (((X)&0xff000000ULL)<<8)+           \
                            (((X)&0xff00000000ULL)>>8)+         \
                            (((X)&0xff0000000000ULL)>>24)+      \
                            (((X)&0xff000000000000ULL)>>40)+    \
                            (((X)&0xff00000000000000ULL)>>56))   

#endif

#define GT_REG_READ(offset, pData)                                          \
*pData = ( (volatile unsigned int)*((unsigned int *)                        \
           (NONE_CACHEABLE | internalRegBaseAddr | (offset))) );            \
*pData = WORD_SWAP(*pData)

//#define GT_REG_WRITE(offset, data)                                          \
//(volatile unsigned int)*((unsigned int *)(NONE_CACHEABLE |                  \
//          internalRegBaseAddr | (offset))) = WORD_SWAP(data)


#define GT_REG_WRITE(offset, data)                                          \
*((unsigned int *)(NONE_CACHEABLE |                  \
          internalRegBaseAddr | (offset))) = WORD_SWAP(data)


typedef enum _bool{false,true} bool;


void gtRegReadFunc(unsigned int offset, unsigned int* pData)
{
  unsigned int add;

  add = NONE_CACHEABLE | internalRegBaseAddr | offset;
  *pData = ( (volatile unsigned int)*((unsigned int *)(add)) );
}

void gtRegWriteFunc(unsigned int offset, unsigned int data)
{
  unsigned int add;

  add = NONE_CACHEABLE | internalRegBaseAddr | offset;
  //( (volatile unsigned int)*((unsigned int *)(add)) ) = WORD_SWAP(data);
  *((unsigned int *)(add)) = WORD_SWAP(data);
  //  *((unsigned int *)(add)) = WORD_SWAP(data);
}

/*****************************************************************************
*
* bool etherReadMIIReg (unsigned int portNumber , unsigned int MIIReg,
* unsigned int* value)
*
* Description
* This function will access the MII registers and will read the value of
* the MII register , and will retrieve the value in the pointer.
* Inputs
* portNumber - one of the 2 possiable Ethernet ports (0-1).
* MIIReg - the MII register offset.
* Outputs
* value - pointer to unsigned int which will receive the value.
* Returns Value
* true if success.
* false if fail to make the assignment.
* Error types (and exceptions if exist)
*/

GT_BOOL gtBspReadMii (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg,
                        unsigned int* value)
{
SMI_REG smiReg;
unsigned int phyAddr;
unsigned int timeOut = 10; /* in 100MS units */
int i;

   /* MSG_PRINT(("gtBspReadMii: start\n"));*/


/* first check that it is not busy */
    GT_REG_READ (ETHER_SMI_REG,(unsigned int*)&smiReg);
    MSG_PRINT(("gtBspReadMii: smiReg %d\n",smiReg));

    if(smiReg & SMI_BUSY) 
    {
        for(i = 0 ; i < SMI_RX_TIMEOUT ; i++);
        do {
            GT_REG_READ (ETHER_SMI_REG,(unsigned int*)&smiReg);
            if(timeOut-- < 1 ) {
    		MSG_PRINT(("gtBspReadMii: BUSY\n"));
                return false;
            }
        } while (smiReg & SMI_BUSY);
    }
/* not busy */

    phyAddr = portNumber;

    smiReg =  (phyAddr << 16) | (SMI_OP_CODE_BIT_READ << 26) | (MIIReg << 21) |
         SMI_OP_CODE_BIT_READ<<26;


    MSG_PRINT(("gtBspReadMii: GT_REG_WRITE 0x%x 0x%x\n",(NONE_CACHEABLE | internalRegBaseAddr | (ETHER_SMI_REG)),smiReg));

    GT_REG_WRITE (ETHER_SMI_REG,*((unsigned int*)&smiReg));
    timeOut = 10; /* initialize the time out var again */
    GT_REG_READ (ETHER_SMI_REG,(unsigned int*)&smiReg);
    if(!(smiReg & READ_VALID)) 
        {
            i=0;
            while(i < SMI_RX_TIMEOUT)
            {
                i++;
            }
        {
        }
        do {
            GT_REG_READ (ETHER_SMI_REG,(unsigned int*)&smiReg);
            if(timeOut-- < 1 ) {
                return false;
            }
        } while (!(smiReg & READ_VALID));
     }
    *value = (unsigned int)(smiReg & 0xffff);
    
    return true;


}

/*****************************************************************************
* 
* bool etherWriteMIIReg (unsigned int portNumber , unsigned int MIIReg,
* unsigned int value)
* 
* Description
* This function will access the MII registers and will write the value
* to the MII register.
* Inputs
* portNumber - one of the 2 possiable Ethernet ports (0-1).
* MIIReg - the MII register offset.
* value -the value that will be written.
* Outputs
* Returns Value
* true if success.
* false if fail to make the assignment.
* Error types (and exceptions if exist)
*/

GT_BOOL gtBspWriteMii (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg,
                       unsigned int value)
{
SMI_REG smiReg;
unsigned int phyAddr;
unsigned int timeOut = 10; /* in 100MS units */
int i;

/* first check that it is not busy */
    GT_REG_READ (ETHER_SMI_REG,(unsigned int*)&smiReg);
    if(smiReg & SMI_BUSY) 
    {
        for(i = 0 ; i < SMI_RX_TIMEOUT ; i++);
        do {
            GT_REG_READ (ETHER_SMI_REG,(unsigned int*)&smiReg);
            if(timeOut-- < 1 ) {
                return false;
            }
        } while (smiReg & SMI_BUSY);
    }
/* not busy */

    phyAddr = portNumber;

    smiReg = 0; /* make sure no garbage value in reserved bits */
    smiReg = smiReg | (phyAddr << 16) | (SMI_OP_CODE_BIT_WRITE << 26) |
             (MIIReg << 21) | (value & 0xffff);


    MSG_PRINT(("gtBspWriteMii: GT_REG_WRITE 0x%x 0x%x\n",(NONE_CACHEABLE | internalRegBaseAddr | (ETHER_SMI_REG)),smiReg));


    GT_REG_WRITE (ETHER_SMI_REG,*((unsigned int*)&smiReg));

    return(true);
}

////#endif//OLD_BS

void gtBspMiiInit(GT_QD_DEV* dev)
{
    return;    
}







//#include <Copyright.h>
/********************************************************************************
* macAddr.c
*
* DESCRIPTION:
*    This sample will demonstrate how to add/delete a static MAC Address 
*    into/from the QuaterDeck MAC Address Data Base.
*        
* DEPENDENCIES:   None.
*
* FILE REVISION NUMBER:
*
*******************************************************************************/
//#include "msSample.h"

/*
 *    Add the CPU MAC address into the QuaterDeck MAC Address database.
 *    Input - None
*/
GT_STATUS sampleAddCPUMac(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_ATU_ENTRY macEntry;

    /* 
     *    Assume that Ethernet address for the CPU MAC is
     *    00-50-43-00-01-02.
    */
    macEntry.macAddr.arEther[0] = 0x00;
    macEntry.macAddr.arEther[1] = 0x50;
    macEntry.macAddr.arEther[2] = 0x43;
    macEntry.macAddr.arEther[3] = 0x00;
    macEntry.macAddr.arEther[4] = 0x01;
    macEntry.macAddr.arEther[5] = 0x02;

    macEntry.portVec = 1 << dev->cpuPortNum;     /* CPU Port number. 7bits are used for portVector. */

    macEntry.prio = 0;            /* Priority (2bits). When these bits are used they override
                                any other priority determined by the frame's data. This value is
                                meaningful only if the device does not support extended priority
                                information such as MAC Queue Priority and MAC Frame Priority */

    macEntry.exPrio.macQPri = 0;    /* If device doesnot support MAC Queue Priority override, 
                                    this field is ignored. */
    macEntry.exPrio.macFPri = 0;    /* If device doesnot support MAC Frame Priority override, 
                                    this field is ignored. */
    macEntry.exPrio.useMacFPri = 0;    /* If device doesnot support MAC Frame Priority override, 
                                    this field is ignored. */

    macEntry.entryState.ucEntryState = GT_UC_STATIC;
                                /* This address is locked and will not be aged out.
                                Refer to GT_ATU_UC_STATE in msApiDefs.h for other option. */

    /* 
     *    Add the MAC Address.
     */
    if((status = gfdbAddMacEntry(dev,&macEntry)) != GT_OK)
    {
        MSG_PRINT(("gfdbAddMacEntry returned fail.\n"));
        return status;
    }

    return GT_OK;
}


/*
 *    Delete the CPU MAC address from the QuaterDeck MAC Address database.
 *    Input - None
*/
GT_STATUS sampleDelCPUMac(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_ATU_ENTRY macEntry;

    /* 
     *    Assume that Ethernet address for the CPU MAC is
     *    00-50-43-00-01-02.
    */
    macEntry.macAddr.arEther[0] = 0x00;
    macEntry.macAddr.arEther[1] = 0x50;
    macEntry.macAddr.arEther[2] = 0x43;
    macEntry.macAddr.arEther[3] = 0x00;
    macEntry.macAddr.arEther[4] = 0x01;
    macEntry.macAddr.arEther[5] = 0x02;

    /* 
     *    Delete the CPU MAC Address.
     */
    if((status = gfdbDelMacEntry(dev,&macEntry.macAddr)) != GT_OK)
    {
        MSG_PRINT(("gfdbDelMacEntry returned fail.\n"));
        return status;
    }

    return GT_OK;
}


/*
 *    Add a multicast MAC address into the QuaterDeck MAC Address database,
 *    where address is 01-00-18-1a-00-00 and frames with this destination has
 *    to be forwarding to Port 1, Port 2 and Port 4 (port starts from Port 0)
 *    Input - None
*/
GT_STATUS sampleAddMulticastAddr(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_ATU_ENTRY macEntry;

    /* 
     *    Assume that we want to add the following multicast address
     *    01-50-43-00-01-02.
    */
    macEntry.macAddr.arEther[0] = 0x01;
    macEntry.macAddr.arEther[1] = 0x50;
    macEntry.macAddr.arEther[2] = 0x43;
    macEntry.macAddr.arEther[3] = 0x00;
    macEntry.macAddr.arEther[4] = 0x01;
    macEntry.macAddr.arEther[5] = 0x02;

    /*
     *     Assume that a packet needs to be forwarded to the second Port (port 1),
     *    the third Port (port 2) and cpu Port, if the frame has destination of
     *    01-00-18-1a-00-00.
    */
    macEntry.portVec =     (1<<1) | /* the second port */
                (1<<2) | /* the third port */
                (1<<dev->cpuPortNum);

    macEntry.prio = 0;            /* Priority (2bits). When these bits are used they override
                                any other priority determined by the frame's data. This value is
                                meaningful only if the device does not support extended priority
                                information such as MAC Queue Priority and MAC Frame Priority */

    macEntry.exPrio.macQPri = 0;    /* If device doesnot support MAC Queue Priority override, 
                                    this field is ignored. */
    macEntry.exPrio.macFPri = 0;    /* If device doesnot support MAC Frame Priority override, 
                                    this field is ignored. */
    macEntry.exPrio.useMacFPri = 0;    /* If device doesnot support MAC Frame Priority override, 
                                    this field is ignored. */

    macEntry.entryState.ucEntryState = GT_MC_STATIC;
                                /* This address is locked and will not be aged out. 
                                Refer to GT_ATU_MC_STATE in msApiDefs.h for other option.*/

    /* 
     *    Add the MAC Address.
     */
    if((status = gfdbAddMacEntry(dev,&macEntry)) != GT_OK)
    {
        MSG_PRINT(("gfdbAddMacEntry returned fail.\n"));
        return status;
    }

    return GT_OK;
}


/*
 *    Delete the Multicast MAC address of 01-00-18-1a-00-00.
 *    Input - None
*/
GT_STATUS sampleDelMulticastAddr(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_ATU_ENTRY macEntry;

    /* 
     *    Assume that Ethernet address for the CPU MAC is
     *    01-50-43-00-01-02.
    */
    macEntry.macAddr.arEther[0] = 0x01;
    macEntry.macAddr.arEther[1] = 0x50;
    macEntry.macAddr.arEther[2] = 0x43;
    macEntry.macAddr.arEther[3] = 0x00;
    macEntry.macAddr.arEther[4] = 0x01;
    macEntry.macAddr.arEther[5] = 0x02;

    /* 
     *    Delete the given Multicast Address.
     */
    if((status = gfdbDelMacEntry(dev,&macEntry.macAddr)) != GT_OK)
    {
        MSG_PRINT(("gfdbDelMacEntry returned fail.\n"));
        return status;
    }

    return GT_OK;
}


/*
 *    This sample function will show how to display all the MAC address
 *    in the ATU.
*/
GT_STATUS sampleShowMacEntry(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_ATU_ENTRY tmpMacEntry;

    MSG_PRINT(("ATU List:\n"));
    memset(&tmpMacEntry,0,sizeof(GT_ATU_ENTRY));

    while(1)
    {
        /* Get the sorted list of MAC Table. */
        if((status = gfdbGetAtuEntryNext(dev,&tmpMacEntry)) != GT_OK)
        {
            return status;
        }

        MSG_PRINT(("(%02x-%02x-%02x-%02x-%02x-%02x) PortVec %#x\n",
                tmpMacEntry.macAddr.arEther[0],
                tmpMacEntry.macAddr.arEther[1],
                tmpMacEntry.macAddr.arEther[2],
                tmpMacEntry.macAddr.arEther[3],
                tmpMacEntry.macAddr.arEther[4],
                tmpMacEntry.macAddr.arEther[5],
                tmpMacEntry.portVec));
    }
    return GT_OK;
}

/*--------------------------------------------------------------------------------------------------*/
/********************************************************************************
* hgVlan.c
*
* DESCRIPTION:
*       Setup the VLAN table of QuaterDeck so that it can be used as a Home 
*        Gateway.
*        
*
* DEPENDENCIES:   None.
*
* FILE REVISION NUMBER:
*
*******************************************************************************/
//#include "msSample.h"
/*
static GT_STATUS sampleHomeGatewayVlan(GT_QD_DEV *dev, 
                               GT_LPORT numOfPorts, 
                       GT_LPORT cpuPort);*/



/*
 *    WAN Port (Port 0) and CPU Port (Port 5) are in VLAN 1 and
 *    all ports (including CPU Port) except WAN Port are in VLAN 2.
 *    1) Set PVID for each port. (CPU port has PVID 2, which is the same as LAN)
 *    2) Set Port Based VLAN Map for each port. (CPU port's VLAN Map is set for all LAN ports)
 *  Notes: 
 *        1) Trailer Mode
 *            When Ethernet Device, which is directly connected to CPU port, sends out a packet
 *            to WAN, DPV in Trailer Tag should have WAN port bit set (bit 0 in this case), and
 *            to LAN, Trailer Tag should be set to 0. 
 *            Restriction : Only one group of VLAN can have multiple ports.
 *        2) Header Mode
 *            When Ethernet Device, which is directly connected to CPU port, sends out a packet
 *            to WAN, VlanTable in Header Tag should have WAN ports bits set (bit 0 in this case), and
 *            to LAN, VlanTable in Header Tag should have LAN ports bits set (bit 1~4 and 6 in this case)
*/
static GT_STATUS sampleHomeGatewayVlan(GT_QD_DEV *dev,GT_LPORT numOfPorts, GT_LPORT cpuPort)
{
    GT_STATUS status;
    GT_U8 index;
    GT_LPORT port,portToSet;
    GT_LPORT portList[MAX_SWITCH_PORTS];

    /* 
     *  set PVID for each port.
     *    the first port(port 0, WAN) has default VID 2 and all others has 1.
     */

    if((status = gvlnSetPortVid(dev,0,2)) != GT_OK)
    {
        MSG_PRINT(("gprtSetPortVid returned fail.\n"));
        return status;
    }

    for (port=1; port<numOfPorts; port++)
    {
        if((status = gvlnSetPortVid(dev,port,1)) != GT_OK)
        {
            MSG_PRINT(("gprtSetPortVid returned fail.\n"));
            return status;
        }
    }

    /* 
     *  set Port VLAN Mapping.
     *    port 0 (WAN) and cpu port are in a vlan 2.
     *    And all the rest ports (LAN) and cpu port are in a vlan 1.
     */

    /* port 0 : set cpuPort only */
    portList[0] = cpuPort;
    if((status = gvlnSetPortVlanPorts(dev,0,portList,1)) != GT_OK)
    {
        MSG_PRINT(("gvlnSetPortVlanPorts returned fail.\n"));
        return status;
    }

    /* set all ports except port 0 and itself */
    for (portToSet=1; portToSet<numOfPorts; portToSet++)
    {
        /* port 0 and cpuPort will be taken cared seperately. */
        if (portToSet == cpuPort)
        {
            continue;
        }

        index = 0;
        for (port=1; port<numOfPorts; port++)
        {
            if (port == portToSet)
            {
                continue;
            }
            portList[index++] = port;
        }

        if((status = gvlnSetPortVlanPorts(dev, portToSet,portList,index)) != GT_OK)
        {
            MSG_PRINT(("gvlnSetPortVlanPorts returned fail.\n"));
            return status;
        }
    }

    /* cpuPort : set all port except cpuPort and WAN port */
    index = 0;
    for (port=1; port<numOfPorts; port++)
    {
        if (port == cpuPort)
        {
            continue;
        }
        portList[index++] = port;
    }

    if((status = gvlnSetPortVlanPorts(dev,cpuPort,portList,index)) != GT_OK)
    {
        MSG_PRINT(("gvlnSetPortVlanPorts returned fail.\n"));
        return status;
    }

    return GT_OK;
}

/*
 *  Get the required parameter from QuarterDeck driver.
 *    Notes: This routine should be called after QuarterDeck Driver has been initialized.
 *        (Refer to Initialization Sample)
*/

GT_STATUS sampleVlanSetup(GT_QD_DEV *dev)
{
    sampleHomeGatewayVlan(dev,dev->numOfPorts, dev->cpuPortNum);

    return GT_OK;
}

/*----------------------------------------------------------------------------------------------------*/


GT_SYS_CONFIG   cfg;
GT_QD_DEV       diagDev;
GT_QD_DEV       *dev=&diagDev;


GT_BOOL gtBspReadMii1 (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg,
                        unsigned int* value)
{
    unsigned short L_registerIndex;
    unsigned long L_value;
    unsigned char dev_id;

    //MSG_PRINT(("gtBspReadMii1: start\n"));

    dev_id = portNumber;//(unsigned char)DEVICE0_ID;
    L_registerIndex = (unsigned short)MIIReg;
    //void ReadPhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long *value)
    ReadPhyReg(dev_id, L_registerIndex, &L_value);
    *value = L_value;

   /* MSG_PRINT(("gtBspReadMii1: %x %x 0x%x.\n",portNumber,L_registerIndex,L_value));*/
    
    return GT_TRUE;
}

GT_BOOL gtBspWriteMii1 (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg,
                       unsigned int value)
{
    unsigned short L_registerIndex;
    unsigned long L_value;
    unsigned char dev_id;

   // MSG_PRINT(("gtBspWriteMii1: start\n"));

    dev_id = portNumber;//(unsigned char)DEVICE0_ID;
    L_registerIndex = (unsigned short)MIIReg;
    L_value = value;

    //void WritePhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long value)
    WritePhyReg(dev_id, L_registerIndex, L_value);

  /*  MSG_PRINT(("gtBspWriteMii1: %x %x %x\n",dev_id,L_registerIndex,L_value));*/

    return GT_TRUE;
}

/*
 *  Initialize the QuarterDeck. This should be done in BSP driver init routine.
 *    Since BSP is not combined with QuarterDeck driver, we are doing here.
*/

GT_STATUS qdStart1(int cpuPort, int useQdSim, int devId) /* devId is used for simulator only */
{
GT_STATUS status;

  /*  MSG_PRINT(("Start qdStart1\n"));
    
     *  Register all the required functions to QuarterDeck Driver.
    */
    memset((char*)&cfg,0,sizeof(GT_SYS_CONFIG));
    memset((char*)&diagDev,0,sizeof(GT_QD_DEV));

    if(useQdSim == 0) /* use EV-96122 */
    {
      /*  MSG_PRINT(("Start qdStart1: useQdSim == 0\n"));*/
        cfg.BSPFunctions.readMii   = gtBspReadMii1;//set for new  function
        cfg.BSPFunctions.writeMii  = gtBspWriteMii1;//set for new  function
#ifdef GT_RMGMT_ACCESS
    cfg.BSPFunctions.hwAccess  = gtBspHwAccess; 
#endif
#ifdef USE_SEMAPHORE
        cfg.BSPFunctions.semCreate = osSemCreate;
        cfg.BSPFunctions.semDelete = osSemDelete;
        cfg.BSPFunctions.semTake   = osSemWait;
        cfg.BSPFunctions.semGive   = osSemSignal;
#else
        cfg.BSPFunctions.semCreate = NULL;
        cfg.BSPFunctions.semDelete = NULL;
        cfg.BSPFunctions.semTake   = NULL;
        cfg.BSPFunctions.semGive   = NULL;
#endif
        gtBspMiiInit(dev);
    }

#ifdef OLD_BS

    else    /* use QuaterDeck Simulator (No QD Device Required.) */
    {
        MSG_PRINT(("Start qdStart1: else\n"));
        cfg.BSPFunctions.readMii   = qdSimRead;
        cfg.BSPFunctions.writeMii  = qdSimWrite;
#ifdef USE_SEMAPHORE
        cfg.BSPFunctions.semCreate = osSemCreate;
        cfg.BSPFunctions.semDelete = osSemDelete;
        cfg.BSPFunctions.semTake   = osSemWait;
        cfg.BSPFunctions.semGive   = osSemSignal;
#else
        cfg.BSPFunctions.semCreate = NULL;
        cfg.BSPFunctions.semDelete = NULL;
        cfg.BSPFunctions.semTake   = NULL;
        cfg.BSPFunctions.semGive   = NULL;
#endif

        qdSimInit(devId,0);
    }
#endif//OLD_BS
  /*  MSG_PRINT(("qdStart1: config cfg\n"));*/

    cfg.initPorts = GT_TRUE;    /* Set switch ports to Forwarding mode. If GT_FALSE, use Default Setting. */
    cfg.cpuPortNum = cpuPort;
#ifdef MANUAL_MODE    /* not defined. this is only for sample */
    /* user may want to use this mode when there are two QD switchs on the same MII bus. */
    cfg.mode.scanMode = SMI_MANUAL_MODE;    /* Use QD located at manually defined base addr */
    cfg.mode.baseAddr = 0x10;    /* valid value in this case is either 0 or 0x10 */
#else
#ifdef MULTI_ADDR_MODE
    cfg.mode.scanMode = SMI_MULTI_ADDR_MODE;    /* find a QD in indirect access mode */
    cfg.mode.baseAddr = 1;        /* this is the phyAddr used by QD family device. 
                                Valid value are 1 ~ 31.*/
#else
    cfg.mode.scanMode = SMI_AUTO_SCAN_MODE;    /* Scan 0 or 0x10 base address to find the QD */
   // cfg.mode.scanMode = SMI_MULTI_ADDR_MODE;
    cfg.mode.baseAddr = 0x10;
#endif
#endif

   /* MSG_PRINT(("qdStart1: start qdLoadDriver\n"));*/
    if((status=qdLoadDriver(&cfg, dev)) != GT_OK)
    {
        MSG_PRINT(("qdLoadDriver return Failed\n"));
        return status;
    }

    //MSG_PRINT(("Device ID     : 0x%x\n",dev->deviceId));
    //MSG_PRINT(("Base Reg Addr : 0x%x\n",dev->baseRegAddr));
    //MSG_PRINT(("No of Ports   : %d\n",dev->numOfPorts));
    //MSG_PRINT(("CPU Ports     : %d\n",dev->cpuPortNum));

    /*
     *  start the QuarterDeck
    */
    if((status=sysEnable(dev)) != GT_OK)
    {
        MSG_PRINT(("sysConfig return Failed\n"));
        return status;
    }

    //MSG_PRINT(("QuarterDeck has been started.\n"));

    return GT_OK;
}



static int swAddVTU(int argc, char* argv[])
{
	int devIdx, pvid, i, dbNum, sid;
	GT_BOOL stuFound;
	GT_U32 portVec, tagVec, unTagVec;
	GT_VTU_ENTRY entry;
	GT_STU_ENTRY stuEntry;
	GT_STATUS status;
	GT_QD_DEV       *pQdDev;

	if((argc < 7) || (argc > 9))
	{
		//showUsage();
		return 1;
	}
	
	devIdx  = (int)strtoul(argv[2], NULL, 16);
	dbNum   = (int)strtoul(argv[3], NULL, 16);
	pvid    = (int)strtoul(argv[4], NULL, 16);
	portVec = (GT_U32)strtoul(argv[5], NULL, 16);
	sid     = (int)strtoul(argv[6], NULL, 16);
	tagVec = unTagVec = 0;
	
	if((pvid <= 0) || (pvid > 0xFFF))
	{
		printf("Option %s is used with invalid parameter\n", argv[1]+1);
		printf("parameter \"pvid\" should be between 1 and 4096\n");
		return 1;
	}

	if(argc > 7)
	{
		i = argc - 7;
		for(i=7; i<argc; i++)
		{
			if(*argv[i] == 'T')
			{
				tagVec = (GT_U32)strtoul(argv[i]+1, NULL, 16);
			}
			else if(*argv[i] == 'U')
			{
				unTagVec = (GT_U32)strtoul(argv[i]+1, NULL, 16);
			}
			else
			{
				//showUsage();
				return 1;
			}
		}
	}

	if (unTagVec & tagVec)
	{
		printf("A port cannot be in both egress Tagged and egress Untagged\n");
		return 1;
	}

	if ((unTagVec | tagVec) && !(portVec & (unTagVec | tagVec)))
	{
		printf("Port for egress Tagged or Untagged should be in portVec\n");
		return 1;
	}
	
	pQdDev = dev;//qddev;

	stuEntry.sid = sid;

	status=gstuFindSidEntry(pQdDev, &stuEntry, &stuFound);
	switch(status)
	{
		case GT_OK:
			break;
		case GT_NO_SUCH:
			for(i=0; i<pQdDev->numOfPorts; i++)
			{
				stuEntry.portState[i] = 0;
			}
			stuFound = GT_TRUE;
			break;

		case GT_NOT_SUPPORTED:
			stuFound = GT_FALSE;
			break;
		default:
			printf("Failed getting STU Entry for sid %i (%i)\n",sid,status);
			stuFound = GT_FALSE;
			break;
	}

	entry.DBNum = (GT_U16)dbNum;
	entry.sid = (GT_U8)sid;
	entry.vid = pvid;
	entry.vidPriOverride = 0;

	for(i=0; i<pQdDev->numOfPorts; i++)
	{
		if((portVec>>i) & 0x1)
		{
			if ((tagVec>>i) & 0x1)
				entry.vtuData.memberTagP[i] = MEMBER_EGRESS_TAGGED;
			else if ((unTagVec>>i) & 0x1)
				entry.vtuData.memberTagP[i] = MEMBER_EGRESS_UNTAGGED;
			else
				entry.vtuData.memberTagP[i] = MEMBER_EGRESS_UNMODIFIED;

			if (stuFound)
			{
				stuEntry.portState[i] = GT_PORT_FORWARDING;
				entry.vtuData.portStateP[i]	= 0;
			}
			else
				entry.vtuData.portStateP[i]	= GT_PORT_FORWARDING;
		}
		else
		{
			entry.vtuData.memberTagP[i] = NOT_A_MEMBER;
			entry.vtuData.portStateP[i]	= 0;
		}
	}
    entry.vidPolicy = GT_FALSE;

	if((status=gvtuAddEntry(pQdDev, &entry)) != GT_OK)
	{
		printf("Adding VTU Entry for vid %i didn't success (%i)\n",pvid,status);
	}
	else
	{
		printf("Adding VTU Entry for vid %i successed\n", pvid);
	}

	if (stuFound != GT_TRUE)
		return status;

	if((status=gstuAddEntry(pQdDev, &stuEntry)) != GT_OK)
	{
		printf("Adding STU Entry for sid %i didn't success (%i)\n",sid,status);
	}
	else
	{
		printf("Adding STU Entry for sid %i successed\n", sid);
	}

	return status;
}



/*******************************************************************************
* 802_1q.c
*
* DESCRIPTION:
*        There are three 802.1Q modes (GT_SECURE, GT_CHECK, and GT_FALLBACK).
*        In GT_SECURE mode, the VID for the given frame must be contained in 
*        the VTU, and the Ingress port must be a member of the VLAN or the 
*        frame will be discarded.
*        In GT_CHECK mode, the VID for the given frame must be contained in 
*        the VTU or the frame will be discarded (the frame will not be 
*        discarded if the Ingress port is not a memeber of the VLAN).
*        In GT_FALLBACK mode, Frames are not discarded if their VID's are not 
*        contained in the VTU. If the frame's VID is contained in the VTU, the 
*        frame is allowed to exit only those ports that are members of the 
*        frame's VLAN; otherwise the switch 'falls back' into Port Based VLAN 
*        mode for the frame (88E6021 Spec. section 3.5.2.1).
*
*        Egress Tagging for a member port of a Vlan has the following three 
*        choices:
*        1) Unmodified,
*        2) Untagged, and
*        3) Tagged
*
*        This sample shows how to utilize 802.1Q feature in the device.
*        For more information, please refer to 88E6021 Spec. section 3.5.2.3
*
* DEPENDENCIES:
*        88E6021 and 88E6063 are supporting this feature.
*
* FILE REVISION NUMBER:
*
*******************************************************************************/

//#include "msSample.h"



GT_STATUS setMarvell(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_DOT1Q_MODE mode;
    GT_VTU_ENTRY vtuEntry;
    GT_STU_ENTRY stuEntry;
    GT_U16 vid;
    GT_LPORT port;
    int i;

    /*
     *    1) Clear VLAN ID Table

    */
    //if((status = gvtuFlush(dev)) != GT_OK)
    //{
    //    MSG_PRINT(("gvtuFlush returned fail.\n"));
    //    return status;
    //}


    /*
     *    2) Add dummy STU.
    */
    gtMemSet(&stuEntry,0,sizeof(GT_STU_ENTRY));
    stuEntry.sid = 1;
    if((status = gstuAddEntry(dev, &stuEntry)) != GT_OK)
    {
        MSG_PRINT(("gstuAddEntry return Failed\n"));
        return status;
    }


    /*
     *    3) Add VLAN ID 2 with all Ports members of the Vlan.
    */
    gtMemSet(&vtuEntry,0,sizeof(GT_VTU_ENTRY));
    vtuEntry.DBNum = 0;
    vtuEntry.vid = 2;
    vtuEntry.sid = 1;
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;
        if(port == 6)
            vtuEntry.vtuData.memberTagP[port] = MEMBER_EGRESS_TAGGED;
        else
            vtuEntry.vtuData.memberTagP[port] = MEMBER_EGRESS_UNTAGGED;
    }

    if((status = gvtuAddEntry(dev,&vtuEntry)) != GT_OK)
    {
        MSG_PRINT(("gvtuAddEntry returned fail.\n"));
        return status;
    }


    /*
     *    4) Enable 802.1Q for all ports as GT_FALLBACK.
    */
    mode = GT_FALLBACK;
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;

        if((status = gvlnSetPortVlanDot1qMode(dev,port, mode)) != GT_OK)
        {
            MSG_PRINT(("gvlnSetPortVlanDot1qMode return Failed\n"));
            return status;
        }
    }

    /*
     *    5) Configure the default vid for each port.
     *    CPU port has PVID 2, and the rest ports have PVID 1.

    */
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;
        if(port == 6)
            vid = 2;
        else
            vid = 1;

        if((status = gvlnSetPortVid(dev,port,vid)) != GT_OK)
        {
            MSG_PRINT(("gvlnSetPortVid returned fail.\n"));
            return status;
        }
    }

    return GT_OK;

}



/*****************************************************************************
* sample802_1qSetup
*
* DESCRIPTION:
*        This routine will show how to configure the switch device so that it 
*        can be a Home Gateway. This example assumes that all the frames are not 
*        VLAN-Tagged.
*        1) to clear VLAN ID Table,
*         2) to enable 802.1Q in SECURE mode for each port except CPU port,
*        3) to enable 802.1Q in FALL BACK mode for the CPU port. 
*        4) to add VLAN ID 1 with member port 0 and CPU port 
*        (untagged egress),
*        5) to add VLAN ID 2 with member the rest of the ports and CPU port 
*        (untagged egress), 
*        6) to configure the default vid of each port:
*        Port 0 have PVID 1, CPU port has PVID 3, and the rest ports have PVID 2.
*        Note: CPU port's PVID should be unknown VID, so that QuarterDeck can use 
*        VlanTable (header info) for TX.
*
*
* INPUTS:
*       None.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       GT_OK               - on success
*       GT_FAIL             - on error
*
* COMMENTS: 
*        WARNING!!
*        If you create just two VLAN for this setup, Trailer mode or Header mode 
*        for the CPU port has to be enabled and Ethernet driver which connects to
*        CPU port should understand VLAN-TAGGING, Trailer mode, or Header mode.
*
*******************************************************************************/
GT_STATUS sample802_1qSetup(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_DOT1Q_MODE mode;
    GT_VTU_ENTRY vtuEntry;
    GT_U16 vid;
    GT_LPORT port;
    int i;

    /*
     *    1) Clear VLAN ID Table
    */
    if((status = gvtuFlush(dev)) != GT_OK)
    {
        MSG_PRINT(("gvtuFlush returned fail.\n"));
        return status;
    }

    /*
     *    2) Enable 802.1Q for each port as GT_SECURE mode except CPU port.
    */
    mode = GT_SECURE;
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;
        if (port == dev->cpuPortNum)
            continue;

        if((status = gvlnSetPortVlanDot1qMode(dev,port, mode)) != GT_OK)
        {
            MSG_PRINT(("gvlnSetPortVlanDot1qMode return Failed\n"));
            return status;
        }
    }

    /*
     *    3) Enable 802.1Q for CPU port as GT_FALLBACK mode
    */
    if((status = gvlnSetPortVlanDot1qMode(dev, dev->cpuPortNum, GT_FALLBACK)) != GT_OK)
    {
        MSG_PRINT(("gvlnSetPortVlanDot1qMode return Failed\n"));
        return status;
    }


    /*
     *    4) Add VLAN ID 1 with Port 0 and CPU Port as members of the Vlan.
    */
    gtMemSet(&vtuEntry,0,sizeof(GT_VTU_ENTRY));
    vtuEntry.DBNum = 0;
    vtuEntry.vid = 1;
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;
        if((i==0) || (port == dev->cpuPortNum))
            vtuEntry.vtuData.memberTagP[port] = MEMBER_EGRESS_UNTAGGED;
        else
            vtuEntry.vtuData.memberTagP[port] = NOT_A_MEMBER;
    }

    if((status = gvtuAddEntry(dev,&vtuEntry)) != GT_OK)
    {
        MSG_PRINT(("gvtuAddEntry returned fail.\n"));
        return status;
    }

    /*
     *    5) Add VLAN ID 2 with the rest of the Ports and CPU Port as members of 
     *    the Vlan.
    */
    gtMemSet(&vtuEntry,0,sizeof(GT_VTU_ENTRY));
    vtuEntry.DBNum = 0;
    vtuEntry.vid = 2;
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;
        if(i == 0)
            vtuEntry.vtuData.memberTagP[port] = NOT_A_MEMBER;
        else
            vtuEntry.vtuData.memberTagP[port] = MEMBER_EGRESS_UNTAGGED;
    }

    if((status = gvtuAddEntry(dev,&vtuEntry)) != GT_OK)
    {
        MSG_PRINT(("gvtuAddEntry returned fail.\n"));
        return status;
    }


    /*
     *    6) Configure the default vid for each port.
     *    Port 0 has PVID 1, CPU port has PVID 3, and the rest ports have PVID 2.
    */
    for(i=0; i<dev->numOfPorts; i++)
    {
        port = i;
        if(i==0)
            vid = 1;
        else if(port == dev->cpuPortNum)
            vid = 3;
        else
            vid = 2;

        if((status = gvlnSetPortVid(dev,port,vid)) != GT_OK)
        {
            MSG_PRINT(("gvlnSetPortVid returned fail.\n"));
            return status;
        }
    }

    return GT_OK;

}


/*****************************************************************************
* sampleAdmitOnlyTaggedFrame
*
* DESCRIPTION:
*        This routine will show how to configure a port to accept only vlan
*        tagged frames.
*        This routine assumes that 802.1Q has been enabled for the given port.
*
* INPUTS:
*       port - logical port to be configured.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       GT_OK               - on success
*       GT_FAIL             - on error
*
* COMMENTS: 
*        Some device support Discard Untagged feature. If so, gprtSetDiscardUntagged
*        function will do the work.
*
*******************************************************************************/
GT_STATUS sampleAdmitOnlyTaggedFrame(GT_QD_DEV *dev,GT_LPORT port)
{
    GT_STATUS status;
    GT_VTU_ENTRY vtuEntry;
    int i;

    /*
     *    0) If device support gprtSetDiscardUntagged, call the function.
    */
    status = gprtSetDiscardUntagged(dev, port, GT_TRUE);
    switch (status)
    {
        case GT_OK:
            MSG_PRINT(("Done.\n"));
            return status;
        case GT_NOT_SUPPORTED:
            MSG_PRINT(("Try other method.\n"));
            break;
        default:
            MSG_PRINT(("Failure accessing device.\n"));
            return status;
    }
            

    /*
     *    1) Add VLAN ID 0xFFF with the given port as a member.
    */
    gtMemSet(&vtuEntry,0,sizeof(GT_VTU_ENTRY));
    vtuEntry.DBNum = 0;
    vtuEntry.vid = 0xFFF;
    for(i=0; i<dev->numOfPorts; i++)
    {
        vtuEntry.vtuData.memberTagP[i] = NOT_A_MEMBER;
    }
    vtuEntry.vtuData.memberTagP[port] = MEMBER_EGRESS_TAGGED;

    if((status = gvtuAddEntry(dev,&vtuEntry)) != GT_OK)
    {
        MSG_PRINT(("gvtuAddEntry returned fail.\n"));
        return status;
    }

    /*
     *    2) Configure the default vid for the given port with VID 0xFFF
    */
    if((status = gvlnSetPortVid(dev,port,0xFFF)) != GT_OK)
    {
        MSG_PRINT(("gvlnSetPortVid returned fail.\n"));
        return status;
    }

    return GT_OK;

}


/*****************************************************************************
* sampleDisplayVIDTable
*
* DESCRIPTION:
*        This routine will show how to enumerate each vid entry in the VTU table
*
* INPUTS:
*       None.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       GT_OK               - on success
*       GT_FAIL             - on error
* COMMENTS: 
*
*******************************************************************************/
GT_STATUS sampleDisplayVIDTable(GT_QD_DEV *dev)
{
    GT_STATUS status;
        GT_VTU_ENTRY vtuEntry;
    GT_LPORT port;    
    int portIndex;
    int vidCounter = 0;

    gtMemSet(&vtuEntry,0,sizeof(GT_VTU_ENTRY));
    vtuEntry.vid = 0xfff;
    if((status = gvtuGetEntryFirst(dev,&vtuEntry)) != GT_OK)
    {
        MSG_PRINT(("gvtuGetEntryCount returned fail.\n"));
        return status;
    }

    MSG_PRINT(("DBNum:%i, VID:%i \n",vtuEntry.DBNum,vtuEntry.vid));

    for(portIndex=0; portIndex<dev->numOfPorts; portIndex++)
    {
        port = portIndex;

        MSG_PRINT(("Tag%i:%#x  ",port,vtuEntry.vtuData.memberTagP[port]));
    }
    
    MSG_PRINT(("\n"));

    while(1)
    {
        if((status = gvtuGetEntryNext(dev,&vtuEntry)) != GT_OK)
        {
            break;
        }

        MSG_PRINT(("DBNum:%i, VID:%i \n",vtuEntry.DBNum,vtuEntry.vid));

        for(portIndex=0; portIndex<dev->numOfPorts; portIndex++)
        {
            port = portIndex;

            MSG_PRINT(("Tag%i:%#x  ",port,vtuEntry.vtuData.memberTagP[port]));
        }
        vidCounter++;
        MSG_PRINT(("\n"));

    }
    
    //if(vidCounter == 0)
    //{
    //    MSG_PRINT(("gvtuGetEntryCount is empty.\n"));
    //}
    return GT_OK;
}

GT_STATUS sampleDisplaySTUTable(GT_QD_DEV *dev)
{
    GT_STATUS status;
    GT_STU_ENTRY stuEntry;
    GT_LPORT port;    
    int portIndex;
    int sidCounter = 0;

    gtMemSet(&stuEntry,0,sizeof(GT_STU_ENTRY));
    stuEntry.sid = 0xfff;
    if((status = gstuGetEntryFirst(dev,&stuEntry)) != GT_OK)
    {
        MSG_PRINT(("gstuGetEntryCount returned fail.\n"));
        return status;
    }

    MSG_PRINT(("SID:%i \n",stuEntry.sid));

    for(portIndex=0; portIndex<dev->numOfPorts; portIndex++)
    {
        port = portIndex;

        MSG_PRINT(("Port%i:%#x  ",port,stuEntry.portState[port]));
    }
    
    MSG_PRINT(("\n"));

    while(1)
    {
        if((status = gstuGetEntryNext(dev,&stuEntry)) != GT_OK)
        {
            break;
        }

        MSG_PRINT(("SID:%i \n",stuEntry.sid));

        for(portIndex=0; portIndex<dev->numOfPorts; portIndex++)
        {
            port = portIndex;

            MSG_PRINT(("Port%i:%#x  ",port,stuEntry.portState[port]));
        }
        sidCounter++;
        MSG_PRINT(("\n"));

    }
    
    
    return GT_OK;
}

GT_STATUS readAllPortsVid(GT_QD_DEV *dev)
{
    GT_STATUS       retVal;
    int portIndex;
    GT_U16   vid;

    for(portIndex=0; portIndex<dev->numOfPorts; portIndex++)
    {
        retVal = gvlnGetPortVid(dev,portIndex,&vid);
	if(retVal != GT_OK)
	{
            MSG_PRINT(("readAllPortVid Failed.\n"));
            return retVal;
    	}

        MSG_PRINT(("Port%i:%#x  ",portIndex,vid));
    }
    MSG_PRINT(("\n"));
    return GT_OK;
}

GT_STATUS readAllPortsDot1qMode
(
    IN GT_QD_DEV        *dev;
)
{
    OUT GT_DOT1Q_MODE   mode;
    GT_STATUS       retVal;
    int portIndex;

    for(portIndex=0; portIndex<dev->numOfPorts; portIndex++)
    {
        retVal = gvlnGetPortVlanDot1qMode(dev,portIndex,&mode);
	if(retVal != GT_OK)
	{
            MSG_PRINT(("readAllPortsDot1qMode Failed.\n"));
            return retVal;
    	}

        MSG_PRINT(("Port%i:%#x  ",portIndex,(GT_U16)mode));
    }
    MSG_PRINT(("\n"));
    return GT_OK;
}


void usage2(void)
{
	printf("    -i Show Port VID\n");
	printf("    -q Show Port 802 Q mode\n");
	printf("    -s Show STU table\n");
	printf("    -m show MAC table\n");
	printf("    -v show Vlan Setup\n");

	return;
}


int main(int argc, char *argv[])
{
	int c;
	int flag = 0;
	GT_STATUS status;
 	unsigned long data[2];

	
	init_mdio();

	
	status = qdStart1(DEVICE0_CPU_PORT, 0x0, DEVICE0_ID); 
	
	setMarvell(dev);


	while((c = getopt(argc, argv, "iqsvm")) != -1) {
		switch(c) {
		case 'i':
			MSG_PRINT(("Show Port VID\n"));
			status = readAllPortsVid(dev);
			flag = 1;
			break;

		case 'q':
			MSG_PRINT(("Show Port 802 Q mode\n"));
			status = readAllPortsDot1qMode(dev);
			flag = 1;
			break;


		case 's':
			MSG_PRINT(("Show STU table\n"));
			status = sampleDisplaySTUTable(dev);
			flag = 1;
			break;

		case 'v':
			MSG_PRINT(("Show VID table\n"));
			//status = sampleVlanSetup(dev);
			status = sampleDisplayVIDTable(dev);
			flag = 1;
			break;
		
		case 'm':
			MSG_PRINT(("Show MAC table\n"));
			status = sampleShowMacEntry(dev);
			flag = 1;
			break;

		default:
			printf("invalid option: %c\n", (char)c);
			usage();
			return -1;
		}

	}

	if (flag == 0) {
		usage2();
		return -1;
	}

}


