#!/bin/bash
#
# SDK base definitions
#
export NFS_SERVER=/nfs/pt/swdev/areas/projects/gr_gpon/projects

#export CROSS_COMPILE=$NFS_SERVER/Toolchain/EABI/armv5-marvell-linux-uclibcgnueabi-soft_i686/bin/arm-marvell-linux-uclibcgnueabi-
export CROSS_COMPILE=/media/doronsa/457cd718-9daa-4ce7-ba1a-42d4d4842eb5/data/doron/workspace/marvel_Linux_v10.93.3.3/SDK_2.6.25-RC78/armv5-marvell-linux-uclibcgnueabi-soft_i686/bin/arm-marvell-linux-uclibcgnueabi-
export PROD_MC=AVANTA_MC_SFU_LB
export PROD_MC_NO_LPBK=AVANTA_MC_SFU
export PROD_MC_REDUCED=AVANTA_MC_RSFU
export PROD_MC_VOIP=AVANTA_MC_SFU_VOIP
export PROD_SFU=AVANTA_SFU
export PROD_SFU_REDUCED=AVANTA_RSFU
export PROD_HGU=AVANTA_HGU

export PROD_HIDDEN_FILE=.curr_product

export PRODUCTS_DIR=Products

export SDK_SOURCES_DIR=Source
export SDK_APPL_DIR=Application
export SDK_KERNEL_DIR=Kernel
export SDK_CONFIG_DIR=ConfigFiles
export SDK_RCS_DIR=init.d
export SDK_XML_CONFIG_DIR=xml_params
export SDK_ROOT_DIR=root
export SDK_BIN_DIR=bin
export SDK_MMP_DIR=MMP
export SDK_UBOOT_DIR=Uboot
export SDK_VAR_DIR=var

export SDK_OUTPUT_DIR=Output
export SDK_ROOTFS_DIR=FileSystem
export SDK_RCVR_IMG_DIR=RecoveryImage

# SDK_PROD is set on run time
export SDK_FS_DIR=$SDK_OUTPUT_DIR/$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR
export SDK_RCVR_DIR=$SDK_OUTPUT_DIR/$PRODUCTS_DIR/$SDK_PROD/$SDK_RCVR_IMG_DIR

export SDK_TOOLS_DIR=Tools
export SDK_DOCS_DIR=Docs

export K_CONFIG_DIR=$PRODUCTS_DIR/$SDK_PROD/$SDK_KERNEL_DIR
export DOTCONFIG_NAME=.config

export ETC_RCS_DIR=etc/$SDK_RCS_DIR
export RCS_CONFIG_DIR=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/$SDK_RCS_DIR
export START_CONFIG_FILE=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/bin/start
export START_CONFIG_FILE_BIN=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/bin
export START_CONFIG_FILE_VAR=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/var
export START_CONFIG_FILE_WEB=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/var/web_root
export START_CONFIG_FILE_ETC=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/etc
export START_CONFIG_FILE_LIB=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/lib

export XML_CONFIG_DIR=$PRODUCTS_DIR/$SDK_PROD/$SDK_ROOTFS_DIR/$SDK_XML_CONFIG_DIR

export XML_PARAMS_DIR=etc/xml_params
export XML_COMMANDS_DIR=etc/xml_commands

export OMCI_TOOL_DIR=tools/omci_tool
export OMCI_TOOL=mv_omci_tool

export SDK_ROOTFS=rootfs.tar
export SDK_SQUASH_FS=rootfs.squashfs
export SDK_SQUASH_FS_PAD=rootfs.squashfs.padded
export SDK_SQUASH_FS_IMAGE=rootfs.squashfs.rcvr.img
export SDK_JFFS2_IMAGE=varfs.jffs2.rcvr.img
export SDK_UIMAGE_PAD=uimage.padded
export SDK_IMAGE=sdk_unified.img

export SDK_CROSS_UCLIBC_NAME=armv5-marvell-linux-uclibcgnueabi-soft_i686

export SDK_APPL=SW_TREE_PUB
export SDK_APPL_SQLITE_DIR=mng/midware/sqlite3
export SDK_APPL_CLISH_DIR=clish/core/clish-0.7.3
export SDK_APPL_DEF_PARAMS_DIR=main/xml_default_params
export SDK_APPL_COMMANDS_DIR=main/xml_commands

export SDK_APPL_BUILD_BIN_DIR=build/bin
export SDK_APPL_BUILD_LIB_DIR=build/lib

export SDK_APPL_OMCI_PKG=omci_pkg
export SDK_APPL_OMCI_PKG_SH=pkgOmciAdd.sh
export SDK_APPL_OAM_PKG=oam_pkg
export SDK_APPL_OAM_PKG_SH=pkgOamAdd.sh

export SDK_APPL_MNG_DIR=mng

export SDK_APM_DIR=$SDK_APPL_MNG_DIR/apm
export SDK_APM_OBJ_DIR=$SDK_APM_DIR/obj

export SDK_OAM_DIR=$SDK_APPL_MNG_DIR/oam
export SDK_OAM_OBJ_DIR=$SDK_OAM_DIR/obj

export SDK_OMCI_DIR=$SDK_APPL_MNG_DIR/omci
export SDK_OMCI_OBJ_DIR=$SDK_OMCI_DIR/obj

export SDK_KERNEL=linux_feroceon-KW2
export SDK_PACKAGE_VERSION_FILE=pkgVersion.txt

export JFFS_DIR=jffs2_fs

export SDK_MMP=mmp
export SDK_MMP_OUTPUT=mmp_runtime
export SDK_MMP_FOR_GW=avanta6560

export SDK_LOGUTIL=/tmp/${USER}_sdk_logutil_`date +%d%m%y`.log.$$
##
##

export SDK_VERSION=2.6.25-RC78
##
##
